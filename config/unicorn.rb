working_directory "/var/rayinda"
pid "/var/rayinda/tmp/pids/unicorn.pid"
stderr_path "/var/rayinda/log/unicorn.log"
stdout_path "/var/rayinda/log/unicorn.log"

listen "/tmp/unicorn.rayinda.sock"
worker_processes 2
timeout 30
